import javax.swing.JOptionPane;
class Collatz{

	public static int par(int numero){
       return numero/2;
	}

	public static int impar(int numero){
		return (numero*3)+1;
	}


    public static void main(String[] args) {

    int valor= Integer.parseInt( JOptionPane.showInputDialog("Ingrese un numero:"));

    while(valor!=1){
    	if (valor%2==0) {
    		valor = par(valor);
    		System.out.print(valor+ " ");
    	}else{
    		valor= impar(valor);
    		System.out.print(valor+ " ");
    	}
    }

	}
}
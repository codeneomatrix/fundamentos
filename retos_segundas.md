## Reto 2
Cree un programa que calcule el valor aproximado del numero pi, usando la serie:

4-(4/3)+(4/5)-(4/7)+(4/9)-(4/11) ......

Tome en cuenta lo siguiente :
- El numero *n* es un numero entre 1 y 100000; el cual establece el numero de
iteraciones para calcular el valor del numero pi.


Ejemplo:
```bash
Entrada
n = 100000
salida
3.1415826535897198

n = 10000
salida
3.1414926535900345

n = 10
salida
3.0418396189294032

n = 1
4.0
```


## Reto 3
El juego de ajedrez fue traido de la india o al menos eso se piensa,
escriba un programa que simule los movimientos de un alfil contra una torre.

Tome en cuenta lo siguiente :
- El programa mostrara en pantalla el tablero con los movimientos de
las piezas.
- Indicara si alguna de las piezas se ha intentado salir del tablero.
- Indicara si alguna de las piezas se ha "comido" a la otra.


## Reto 4
Los numeros amigos, son aquellos en donde los divisores propios de uno de ellos
al sumarlos dan como resultado el segundo numero. Escriba un programa que imprima
en pantalla la sucesion alícuota de un numero cualquiera hasta el cero o hasta
que aparezca un ciclo.

Tome en cuenta lo siguiente :
- Los numeros amigos y la sucesion alicuota se explican en el siguiente video:
https://www.youtube.com/watch?v=yCR0hi48rhk
- El numero *n* es un numero entre 1 y 50.

Ejemplo:
```bash
Entrada
n = 30
salida
30 42 54 66 78 90 144 259 45 33 15 9 4 3 1 0
Entrada
n = 10
salida
10 8 7 1 0
Entrada
n = 12496
salida
12496 14288 15472 14536 14264 12496
Entrada
n = 1264460
salida
1264460 1547860 1727636 1305184 1264460
```
### Examen unidad 4
Los numero felices son aquellos que al elevar al cuadrado las cifras que los conforman y sumar esos productos, despues de un determinado numero de iteraciones el resultado sera 1.
Dado un numero cualquiera (del 1 hasta el 998) introducido por linea de comandos, imprimir en pantalla si el numero es feliz con un 1 o no lo es con un 0.


Tome en cuenta lo siguiente :
- Los numeros felices se explican en el siguiente video:
https://www.youtube.com/watch?v=R2D8SuRjtnQ
- El numero *n* es un numero entre 1 y 998.


Ejemplo:
```bash
Entrada
n = 44
salida
1

Entrada
n = 98
salida
0

Entrada
n = 998
salida
1
```

### Condiciones de entrega
Subira el codigo a su cuenta de github, el cual tendra lo siguiente

nombre: main[numerodecontrol].java,

ejemplo: retomain12161219.java

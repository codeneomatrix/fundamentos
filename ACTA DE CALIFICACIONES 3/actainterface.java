import java.util.Arrays;
import javax.swing.JOptionPane;
import java.io.*; 
import javax.swing.*;

public class actainterface extends javax.swing.JFrame {
      static String clave,grupointro,numero="",salida="",texto="",clavemateria;
	static int busqueda,busqueda2,busqueda3,busqueda4,k=0;
	static estudiante[] arreEstudiante =new estudiante[197];
	static materia[] arreMaterias =new materia[13];
	static inscrito[] arreIns =new inscrito[213];
	static docente[] arredocen =new docente[20];
	static oferta[] arreofer =new oferta[21];
        
    public actainterface() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
  
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jToggleButton2 = new javax.swing.JToggleButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        
        jLabel5 = new javax.swing.JLabel();
		 jLabel6 = new javax.swing.JLabel();
     
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ACTA DE CALIFICACIONES");
        setBackground(new java.awt.Color(255, 255, 255));
        setFont(new java.awt.Font("Adobe Arabic", 0, 10)); 
        getContentPane().setLayout(null);

        jLabel1.setText("MATERIA:");
        jLabel1.setToolTipText("");
        jLabel1.setEnabled(false);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 80, 330, 20);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "NUMERO DE CONTROL", "NOMBRE"
            }
        ));
        jTable2.setEnabled(false);
        jScrollPane2.setViewportView(jTable2);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(80, 160, 430, 350);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "CALCULO DIFERENCIAL", "CALCULO VECTORIAL", "CULTURA EMPRESARIAL", "DESARROLLO SUSTENTABLE","ESTRUCTURA DE DATOS","FUNDAMENTOS DE PROGRAMACION","FUNDAMENTOS DE TELECOMUNICACIONES","POO","QUIMICA","SIMULACION","TALLER DE BASE DE DATOS","TALLER DE ETICA","TOPICOS AVANZADOS DE PROGRAMACION" }));
        getContentPane().add(jComboBox1);
        jComboBox1.setBounds(40, 50, 300, 20);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ISCA", "ISCB","ISCU"}));
        getContentPane().add(jComboBox2);
        jComboBox2.setBounds(340, 50, 70, 20);

        jToggleButton2.setText("BUSCAR");
        
        jToggleButton2.setActionCommand("BUSCAR");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jToggleButton2);
        jToggleButton2.setBounds(420, 50, 83, 23);

        jLabel2.setText("GRUPO:");
        jLabel2.setEnabled(false);
        getContentPane().add(jLabel2);
        jLabel2.setBounds(360, 80, 170, 14);

        jLabel3.setText("DOCENTE:");
        jLabel3.setEnabled(false);
        getContentPane().add(jLabel3);
        jLabel3.setBounds(40, 110, 290, 14);

        jLabel4.setText("AULA:");
        jLabel4.setEnabled(false);
        getContentPane().add(jLabel4);
        jLabel4.setBounds(360, 110, 170, 14);
        
         jLabel6.setText("HORARIO:");
        jLabel6.setEnabled(false);
        getContentPane().add(jLabel6);
        jLabel6.setBounds(40, 140, 170, 14);
        
        

        jButton1.setText("+");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(510, 50, 50, 23);
        
        jToggleButton1.setText("GUARDAR EN ARCHIVO");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jToggleButton1);
        jToggleButton1.setBounds(320, 520, 185, 23);
     
        

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); 
        jLabel5.setText("ACTA DE CALIFICACIONES ");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 10, 240, 17);

        pack();
    }
    
    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {
         try{
    
    FileWriter fstream = new FileWriter("ARCHIVOACTA.txt",false);
        BufferedWriter out = new BufferedWriter(fstream);

    out.write(texto);
   
    out.close();
    }catch (Exception e){
      System.err.println("Error: " + e.getMessage());
    }
    }

   private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {  
   	 jLabel1.setText("MATERIA:");
        jLabel1.setToolTipText("");
          jLabel2.setText("GRUPO:");
        jLabel2.setEnabled(false);
        jLabel3.setText("DOCENTE:");
        jLabel3.setEnabled(false);
        jLabel4.setText("AULA:");
        jLabel4.setEnabled(false);
         jLabel6.setText("HORARIO:");
        jLabel6.setEnabled(false);
        
      	limpiar();                                            
         Arrays.sort(arreMaterias);
         String nm =  (String)jComboBox1.getSelectedItem();
			materia introduce = new materia(nm);
		
			    grupointro =(String)jComboBox2.getSelectedItem();
			int bus1 =  Arrays.binarySearch(arreMaterias,introduce);
			  

			  if(bus1<0){
	               JOptionPane.showMessageDialog(null, "lA MATERIA NO SE ENCUENTRA", "no",JOptionPane.ERROR_MESSAGE);
	               
	           }

			  clave= arreMaterias[bus1].clave();
			  jLabel1.setText("MATERIA:"+" "+arreMaterias[bus1].materia());
			   jLabel1.setEnabled(true);
			  salida+="\t"+arreMaterias[bus1].materia();

			 Arrays.sort(arreofer);
			   oferta busca = new oferta(clave,grupointro);
			   busqueda =  Arrays.binarySearch(arreofer,busca);

			  if(busqueda<0){
	              JOptionPane.showMessageDialog(null, "NO SE ENCUENTRA EL GRUPO", "no",JOptionPane.ERROR_MESSAGE);
	          }

			  String grupo= arreofer[busqueda].grupo();
			  salida+="\t"+"  GRUPO:"+grupo+"\n";
			  jLabel2.setText("GRUPO: "+grupo);
        	  jLabel2.setEnabled(true);

			  String targeta = arreofer[busqueda].targeta();
			  clavemateria = arreofer[busqueda].clave();

			  Arrays.sort(arredocen);
			   docente buscad = new docente(targeta);
			   busqueda2 =  Arrays.binarySearch(arredocen,buscad);

			  if(busqueda2<0){
	              JOptionPane.showMessageDialog(null, "NO SE ENCUENTRA EL DOCENTE", "no",JOptionPane.ERROR_MESSAGE);
	          }
			  String nombredoc= arredocen[busqueda2].profesor();
			  salida+="\t\n"+"DOCENTE: "+nombredoc+" AULA:  "+arreofer[busqueda].salon()+"\n"+"HORARIO: "+arreofer[busqueda].horario()+":00  horas \n\n";
                jLabel3.setText("DOCENTE: "+nombredoc);
               jLabel3.setEnabled(true);
                jLabel4.setText("AULA: "+arreofer[busqueda].salon());
               jLabel4.setEnabled(true);
               jLabel6.setText("HORARIO: "+arreofer[busqueda].horario()+"  horas \n\n");
               jLabel6.setEnabled(true);
			  salida+="\t\n NUMERO DE CONTROL  NOMBRE    \n";
		

			  for(int i=0;i<arreIns.length;i++){
				   if((arreIns[i].clave()+arreIns[i].grupo()).compareTo(clavemateria+grupointro)==0){
		        	  numero = arreIns[i].numerocon();
		        	  for(int j=0;j<arreEstudiante.length;j++){
		        		  if((arreEstudiante[j].ncontrol()).compareTo(numero)==0){
		        			   jTable2.setValueAt(arreEstudiante[j].ncontrol(),k,0);
		        			   jTable2.setValueAt(arreEstudiante[j].toString(),k,1);
		        			  salida+= "\t\n"+arreEstudiante[j].ncontrol()+"                   "+arreEstudiante[j].toString()+"\n";
		        			  k++;
		        	        }
		        		  }
		        	  }

		  		        	}
              k=0;
              


			  //JOptionPane.showMessageDialog(null, salida, "alumnosTOTAL",JOptionPane.ERROR_MESSAGE);
         texto=salida;
        salida="";
        
    }
       public  void limpiar(){
   		
        	for(int j=0;j<30;j++){
        jTable2.setValueAt( "",j ,0 );
        jTable2.setValueAt( "",j ,1 );
       
        }
        k=0;
       
   }    
     private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
       String leen = null;
       String sal="";
       
       JFileChooser selectorArchivo = new JFileChooser();
       selectorArchivo.setFileSelectionMode(JFileChooser.FILES_ONLY); 
       	
       	int resultado = selectorArchivo.showSaveDialog(this);
       	
       	if (resultado == JFileChooser.CANCEL_OPTION){
       		return;
       	}
       	File nombreArchivo = selectorArchivo.getSelectedFile();
       	
       	if(nombreArchivo== null || nombreArchivo.getName().equals("")){
       		JOptionPane.showMessageDialog(this,"nombre de archivoinvalido","nombre de archivo invalido",JOptionPane.ERROR_MESSAGE);
       	}
       	
       	else{
       		try{
       		
       		 BufferedReader nuev = new  BufferedReader(new FileReader(new File(nombreArchivo.getName())));
       		 while ( (leen=nuev.readLine()) != null ) {
       		 	sal+=leen+"\n"; 
       		 }
       		 JOptionPane.showMessageDialog(null, sal, "alumnosTOTAL",JOptionPane.ERROR_MESSAGE);}
       	catch (IOException e) {  
           System.out.println("Uh oh, error IOException!" + e.getMessage()); }
       	}
       	}
       
    
    public static void main(String args[]) {
       
         String intro = null;
	   int a = 0;
	   String lee = null;
	   int b = 0;
       String lee2 = null;
	   int c = 0;
       String lee3 = null;
	   int d = 0;
	   String lee4 = null;
	   int k = 0; 
	   
      		try { 
      			
	BufferedReader alu = new  BufferedReader(new FileReader(new File("Estudiante.txt")));
      while ( (intro=alu.readLine()) != null ) { 
         String[] alum=intro.split(";");
         arreEstudiante[a] = new estudiante(alum[1],alum[0]);
            a++;}  
          	
          
          	BufferedReader mat = new  BufferedReader(new FileReader(new File("materias.txt")));
      while ( (lee=mat.readLine()) != null ) { 
         String[] mater=lee.split(";");
        arreMaterias[b] = new materia(mater[0],mater[1]);
            b++;} 
            	
            BufferedReader inscri = new  BufferedReader(new FileReader(new File("inscrito.txt")));
      while ( (lee2=inscri.readLine()) != null ) { 
         String[] ins=lee2.split(";");
          arreIns[c] = new inscrito(ins[2],ins[0],ins[1]);
           	
            c++;} 
         
          BufferedReader docen = new  BufferedReader(new FileReader(new File("docente.txt")));
      while ( (lee3=docen.readLine()) != null ) { 
         String[] dos=lee3.split(";");
        
          arredocen[d] = new docente(dos[0],dos[1]);
           d++;}
           
            BufferedReader ofert = new  BufferedReader(new FileReader(new File("oferta.txt")));
      while ( (lee4=ofert.readLine()) != null ) { 
         String[] ofr=lee4.split(";");
      
          arreofer[k] = new oferta(ofr[0],ofr[1],ofr[2],ofr[3],ofr[4]);
           k++;}
            
          
          }catch (IOException e) {  
           System.out.println("Uh oh, error IOException!" + e.getMessage()); }

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(actainterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(actainterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(actainterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(actainterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new actainterface().setVisible(true);
            }
        });
    }
   
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable2;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    
}
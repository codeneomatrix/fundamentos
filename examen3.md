### Examen unidad 3
El juego de ajedrez fue traido de la india o al menos eso se piensa,
escriba un programa que simule los movimientos de un alfil o de una torre. Al
programa se le pasara una lista de valores Y, X, a, b, c, d e imprimira en la
pantalla el numero de la fila y la columna donde la pieza fue movida al final
o devolvera un "-01" si es que el ultimo moviento pretendio salirse del tablero
o no es un moviemento valido.

Tome en cuenta lo siguiente :
- Los numeros Y, X son la fila y la columna inicial de la pieza por lo
  que el valor de ambos esta en el rango de 0 al 7 .
- Los valores a, b refieren a el numero de filas y columnas que se
  moveran a partir de la posicion inicion inicial.
- Los valores c, d refieren a el numero de filas y columnas que se
  moveran a partir de la ultima posicion de la pieza.


Ejemplo:
```bash
Entrada (alfil)
["3","3","-3","1","4","-4"]
salida
-01

Entrada (torre)
["0","0","7","0","0","4"]
salida
74

```

### Condiciones de entrega
Subira el codigo a su cuenta de github, el cual tendra lo siguiente

nombre: main[numerodecontrol].java,

ejemplo: retomain12161219.java
